import db
import random

# gibt die MealID eines Gerichtes zurück, das vorgeschlagen wird
def getSuggestion(name):

    favList = db.getPersonsFavoriteMeals(name,0.5)      # gibt nur positive bewertete(>=0.5) Gerichte zurück
    favoriteMeals = [favList[i][6] for i in range(len(favList))]
    favoriteMeals.reverse()

    todaysList = db.getMealsToday()
    todaysMeals = [todaysList[i][1] for i in range(len(todaysList))]
    suggestions = []

    for i in range(len(todaysMeals)):
        if todaysMeals[i] in favoriteMeals:
            suggestions.append(todaysMeals[i])

    if suggestions:                                     # Ist genau dann True, falls die Liste "Suggestions" nicht leer ist
        return todaysList[todaysMeals.index(suggestions[0])][0]
    return random.choice(todaysList)[0]
# Todo: speicher das sinnvoll in die Datenbank, z.B. Mittelwert, anhand bestimmter Kriterien, steckt Kartoffel auch in anderen Speisen und das Essen jetzt beeinflusst das mögen dieser Speisen, mag ich vegane oder vegetarische Speisen scheinbar sehr usw. usf....
def calcNewLikes(mealId,value,name):
    nameId = db.getIdOfPerson(name)
    likes = db.getLikesOfPerson(nameId)
    likesIds = [str(likes[i][1]) for i in range(len(likes))]
    if str(mealId) in likesIds:
        index = likesIds.index(mealId)
        newValue = (float(value) + float(likes[index][2])) / 2
    else:
        newValue = value
    db.insertLikes(nameId,mealId,newValue)
