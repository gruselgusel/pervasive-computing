import urllib.request,sys
# Mensa Neues Palais 55, Mensa Golm 61, Mensa Griebnitzsee 62
def getMeals():
    result=[]
    date=""
    mensa_nr = [55,61,62]

    for j in range(len(mensa_nr)):
        fp=urllib.request.urlopen('https://openmensa.org/c/' + str(mensa_nr[j]) + date)
        mybytes=fp.read()
        mystr=mybytes.decode("utf8")
        fp.close()

        mystr = mystr.split('</li></ul></li></ul></li></ul>')
        mystr = mystr[0].split('ab 14:00 Uhr')

        angebote = mystr[0].split('<p>')[1:]

        for i in range(len(angebote)):
            
            if len(angebote[i].split('<span>Studenten</span>')) > 1:
                essen = angebote[i].split('</p>')[0].replace('\n', ' ').replace('&quot;','"').replace('  ',' ')
                preis = angebote[i].split('<span>Studenten</span>')[1].split(' €')[0]
                if len(essen) > 4:
                    result.append(essen)
                    result.append(preis)
    return result
