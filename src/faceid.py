import face_recognition
import os,sys

#def main():
    #if len(sys.argv)<2:
        #print("Input a path of a Picture")
        #return
    #calculate(sys.argv[1])

def calculate(image,path):
    unknown=image#path[0:len(path)-len("known")]+sys.argv[1]
    unknown_image=face_recognition.load_image_file(unknown)
    unknown_image_encoding=face_recognition.face_encodings(unknown_image)[0]

    known=os.listdir(path)

    loaded_images=[]
    loaded_encodings=[]
    u=0

    for i in range(0,len(known)):
            loaded_images.append(face_recognition.load_image_file(path+"/"+known[i]))
            loaded_encodings.append(face_recognition.face_encodings(loaded_images[i])[0])
            if (face_recognition.compare_faces([loaded_encodings[i]],unknown_image_encoding)[0]):
                    u=1
                    return known[i].split(".")[0]
            
    if u==0:
            return "No Matches!"

#Main()
