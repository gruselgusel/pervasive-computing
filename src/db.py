import sqlite3 
import os,sys
import mensaessen as me
DBPATH="../data.sqlite3"

def init():
    conn=sqlite3.connect(DBPATH)
    c=conn.cursor()
    c.execute("create table if not exists persons(id int primary key,name text unique)")
    c.execute("create table if not exists meals(id int primary key,name text unique,vegetarian int, vegan int,price real) ")
    c.execute("create table if not exists likes(person int,meal int,value real,primary key(person,meal))")
    conn.commit()
    conn.close()
def dbExecute(query,value):
    conn=sqlite3.connect(DBPATH)
    c=conn.cursor()
    c.executemany(query,value)
    conn.commit()
    conn.close()

def dbGetValue(query,value):
    conn=sqlite3.connect(DBPATH)
    c=conn.cursor()
    c.execute(query,value)
    result=c.fetchone()
    conn.commit()
    conn.close()
    return result
def dbGetValues(query,value):
    conn=sqlite3.connect(DBPATH)
    c=conn.cursor()
    c.execute(query,value)
    result=c.fetchall()
    conn.commit()
    conn.close()
    return result
def getIdOfPerson(name):
    id=dbGetValue("select id from persons where name=?",(name,))
    if id==None:
        return ""
    return id[0]

def getLikesOfPerson(id):
    result=dbGetValues("select likes.person,likes.meal,likes.value,meals.name from likes left join meals on likes.meal=meals.id where likes.person=?",(id,))
    if result==None:
        return ""
    return result

def getMaxId(tableName):
    id=dbGetValue("select max(id)+1 from {}".format(tableName),"")
    if (isinstance(id[0],int) ):
        id=id[0]
    else:
        id=1
    return id

def insertMealsToday():
    meals=me.getMeals()
    mealList=[]
    id=getMaxId("meals")
    for i in range(0,len(meals),2):

################################## Hier ein vernünftiges Reasoning einbauen, ob vegan oder vegetarisch!!!
        if (meals[i].find("vegan")!=-1):
            vegan=1
            vegetarisch=1
        elif (meals[i].find("vegetarisch")!=-1):
            vegan=0
            vegetarisch=1
        else:
            vegan=0
            vegetarisch=0
##################################

        mealList.append((id,meals[i],vegetarisch,vegan,meals[i+1]))
        id+=1
    dbExecute("insert or ignore into meals values(?,?,?,?,?)",mealList)

#Gibt dir alle heutigen Gerichte als Liste
def getMealsToday():
    meals=me.getMeals()
    result=[]
    for i in range(len(meals)):
        value=dbGetValue("select * from meals where name=?",(meals[i],))
        if (value!=None):
            result.append(value)
    return result
#Liste von (id,name)
def listPersons():
    result=dbGetValues("select * from persons","")
    return result

#Liste von (id,name,vegetarian,vegan,price)
def listMeals():
    result=dbGetValues("select * from meals","")
    return result

#name ist der textliche name
def insertPerson(name):
    id=getMaxId("persons")
    dbExecute("insert or ignore into persons values(?,?)",[(id,name),])

#nameId ist personen id
#mealId ist meal id
#value ist eine zahl zwischen 0 und 1
def insertLikes(nameId,mealId,value):
    dbExecute("insert or replace into likes values(?,?,?)",[(nameId,mealId,value),])

#gibt eine aufsteigend sortierte Liste zurück, von Gerichten einer Person, die eine Bewertung  haben, die größer gliech dem eingegeben Wert ist
def getPersonsFavoriteMeals(name,value=0):
    result=dbGetValues("select * from persons left join likes on persons.id=likes.person left join meals on meals.id=likes.meal where likes.value>=? and persons.name=? order by likes.value asc",(value,name))
    return result
def getMealFromId(nameId):
    result=dbGetValue("select name from meals where id=?",(nameId,))
    if result==None:
        return "No Meal matches Id"
    return result[0]


init()
insertPerson("Amadeus")
insertPerson("Obama")
insertPerson("Christian")
insertMealsToday() # Online Funktion


