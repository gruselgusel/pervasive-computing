import db
import reasoning
import dlib
import faceid,sys,os
def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def getProposition(imageParam):
    knownFaces="../known"
    if (imageParam[0:1]!="./"):
        image="./"+imageParam
    else:
        image=imageParam
    try:
        name=faceid.calculate(image,knownFaces)
        return (reasoning.getSuggestion(name),name)
    except:
        print("Kenne ich nicht!")
        return ("","")

if (len(sys.argv)==1):
    print("Ein Bild als Argument fehlt")
else:
    result=getProposition(sys.argv[1])
    mealId=result[0]
    name=result[1]
    if mealId=="":
        exit()
    try:
        f=open('log','r')
        lastId=f.readline()
        f.close()
    except:
        lastId=""
    print("Du bist:",name)
    if lastId!="":
        print("Please rate your last meal: {} \n".format(db.getMealFromId(lastId)))
        rating=input("Enter a value between 0 and 1:")
        if (isFloat(rating) and float(rating)<=1 and float(rating)>=0):
            reasoning.calcNewLikes(lastId,rating,name)
        else:
            print("no valid value")
            exit()
    mealId=getProposition(sys.argv[1])[0]
    print(db.getMealFromId(mealId))
    f=open("log","w")
    f.write(str(mealId))
    f.close()

